<?php
    require_once("animal.php");
    class Frog extends Animal
    {
        public function __construct($kodok){
            $this->name = $kodok;
        }
        public function jump()
        {
            echo "Hop hop";
        }
    }

?>